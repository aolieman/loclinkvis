SERVE_GEO_DEMO = False
SERVE_MUNICIPAL = True

OSM_NAME_TAGS = ['name', 'nat_name', 'reg_name', 'old_name', 'alt_name', 'short_name']
NL_LANGUAGES = ['nl', 'frr', 'fry', 'fy', 'li', 'nds', 'nds-nl', 'zea']
ACORA_TREE_SIZE = 10000

NL_NAT_DETECTION = {
    'nation_name': u'Nederland',
    'lon_range': (3, 7),  # (3.3580370117, 7.2274985)
    'lat_range': (50, 53),  # (50.7475637428, 53.5494619435)
    'neighbors': (u'Belgi\xeb', u'Duitsland', u'Engeland', u'Luxemburg'),
    'cardinal_directions': ('noord', 'zuid', 'oost', 'west')
}

TOKENIZER_MODEL = 'tokenizers/punkt/dutch.pickle'

NL_SPATIAL_ANALYSIS = {
    'line_multiplier': 142,
    'area_multiplier': 7630
}

MIN_HOOD_SIMILARITY = 0.2
