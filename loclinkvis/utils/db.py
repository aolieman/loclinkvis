from loclinkvis.gazetteer.models import Feature


def get_feature_collection(mongo_query, collection=None):
    features = []
    if collection is None:
        collection = Feature.collection

    cursor = collection.find(mongo_query)
    for feature in cursor:
        try:
            feature.id = str(feature.pop('_id'))
        except AttributeError:
            pass
        features.append(feature)

    feature_collection = {
            'type': 'FeatureCollection',
            'features': features
    }
    return feature_collection
