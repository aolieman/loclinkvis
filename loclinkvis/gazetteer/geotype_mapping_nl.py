
def by_admin_level(admin_level):
    lvl = int(admin_level)
    if lvl == 0:
        raise ValueError('admin_level must be a positive integer, but the input evaluates to 0')

    mapping = {
        1: u'super_national',
        2: u'country',          # koninkrijk
        3: u'country',
        4: u'province',
        5: u'province',         # waterschap
        6: u'province',         # plusregio
        7: u'municipality',     # collaboration of municipalities
        8: u'municipality',
        9: u'neighborhood',     # stadsdeel
        10: u'municipality',    # woonplaats
        11: u'neighborhood'
    }

    geo_type = mapping.get(lvl)

    if not geo_type:
        raise ValueError('admin_level {} is not defined in the mapping'.format(lvl))

    return geo_type


def by_class_and_type(osm_class, osm_type):

    mapping = {
        None: {                 # this class should have few instances (OSM noise)
            u'building':        u'building',
            u'boundary':        u'municipality',
            u'multipolygon':    u'water',
            u'route':           u'road'
        },
        u'aeroway':     u'building',
        u'amenity':     u'building',
        u'boundary':    u'water',       # dubious, but lexical context is similar
        u'bridge':      u'building',
        u'building':    u'building',
        u'highway':     u'road',
        u'historic':    u'building',
        u'landuse': {
            u'military':        u'building',
            u'village_green':   u'building',
            u'reservoir':       u'building',
            u'cemetery':        u'neighborhood',
            u'retail':          u'building',
            u'grass':           u'building',
            u'residential':     u'neighborhood',
            u'quarry':          u'building',
            u'farmyard':        u'building',
            u'industrial':      u'building',
            u'forest':          u'building'
        },
        u'leisure': {
            u'golf_course':     u'building',
            u'marina':          u'water',
            u'recreation_ground': u'building',
            u'stadium':         u'building',
            u'park':            u'water',
            u'playground':      u'building',
            u'common':          u'building',
            u'nature_reserve':  u'water',
            u'sports_centre':   u'building',
            u'pitch':           u'building',
            u'garden':          u'building',
            u'slipway':         u'building'
        },
        u'natural':     u'water',
        u'place': {
            u'country':         u'country',
            u'island':          u'water',
            u'locality':        u'municipality',
            u'village':         u'municipality',
            u'region':          u'province',
            u'city':            u'municipality',
            u'suburb':          u'neighborhood',
            u'house':           u'building',
            u'town':            u'municipality',
            u'hamlet':          u'municipality',
            u'neighbourhood':   u'neighborhood'
        },
        u'railway':     u'road',
        u'shop':        u'building',
        u'tourism':     u'building',
        u'tunnel':      u'building',
        u'waterway':    u'water'
    }

    gt_or_type_dict = mapping.get(osm_class)

    if not gt_or_type_dict:
        raise ValueError()

    elif isinstance(gt_or_type_dict, dict):
        geo_type = gt_or_type_dict.get(osm_type)

        if not geo_type:
            raise ValueError()

    else:
        geo_type = gt_or_type_dict

    return geo_type


def by_other_properties(feature_properties):
    props = feature_properties.copy()
    geo_type = u'building'  # default type

    if props.get('place'):
        geo_type = by_class_and_type('place', props['place'])
    elif props.get('toponym'):
        geo_type = u'water'
    elif props.get('waterway'):
        geo_type = u'water'
    elif props.get('natural'):
        geo_type = u'water'
    elif props.get('operator') and not props.get('power'):
        geo_type = u'water'
    elif props.get('highway'):
        geo_type = u'road'
    elif props.get('railway'):
        geo_type = u'road'

    return geo_type
