#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from itertools import cycle

import minimongo
import regex
import requests
from contextlib import contextmanager
from pymongo import ASCENDING, GEOSPHERE
from pymongo.errors import InvalidOperation, OperationFailure, DuplicateKeyError

import geotype_mapping_nl as gt_map
import osm_models
from loclinkvis.exceptions import CannotSave, CredentialsInvalid
from loclinkvis.settings import *
from loclinkvis.geocoding.spatial_analysis import compute_llv_distance
from loclinkvis.geocoding.subsequence_tests import contains_subsequence
from loclinkvis.utils.web import rate_limited

logger = logging.getLogger(__name__)


class Node(osm_models.Node):
    class Meta:
        collection = "node"
        indices = (
            minimongo.Index([("tags.wikidata", ASCENDING)], name='tags.wikidata', sparse=True),
            minimongo.Index([("tags.wikipedia", ASCENDING)], name='tags.wikipedia', sparse=True),
            minimongo.Index([("tags.place", ASCENDING)], name='tags.place', sparse=True)
        )

    def get_geometry(self):
        coordinates = [self.lon, self.lat]
        geo_type = "Point"
        return geo_type, coordinates


class Way(osm_models.Way):
    class Meta:
        collection = "way"
        indices = (
            minimongo.Index([("tags.name", ASCENDING)], name='tags.name', sparse=True),
            minimongo.Index([("tags.wikidata", ASCENDING)], name='tags.wikidata', sparse=True),
            minimongo.Index([("tags.wikipedia", ASCENDING)], name='tags.wikipedia', sparse=True),
            minimongo.Index([("tags.boundary", ASCENDING)], name='tags.boundary', sparse=True),
            minimongo.Index([("tags.place", ASCENDING)], name='tags.place', sparse=True)
        )

    def get_geometry(self):
        node_dict = {
            node._id: node
            for node in Node.collection.find(
                {'_id': {'$in': self.nodes}}
            )
        }
        coordinates = [
            [node_dict[nid].lon, node_dict[nid].lat]
            for nid in self.nodes
        ]
        if self.nodes[0] == self.nodes[-1]:
            geo_type = "Polygon"
            coordinates = [coordinates]
        else:
            geo_type = "LineString"
        return geo_type, coordinates


class Relation(osm_models.Relation):
    class Meta:
        collection = "relation"
        indices = (
            minimongo.Index([("tags.name", ASCENDING)], name='tags.name'),
            minimongo.Index([("tags.wikidata", ASCENDING)], name='tags.wikidata', sparse=True),
            minimongo.Index([("tags.wikipedia", ASCENDING)], name='tags.wikipedia', sparse=True),
            minimongo.Index([("tags.boundary", ASCENDING)], name='tags.boundary', sparse=True)
        )

    def get_geometry(self):
        rel_type = self.tags.get('type')
        if rel_type in {'boundary', 'multipolygon', 'building', 'water'}:
            # make (multi)polygon
            outer = []
            inner = []
            polygon = []
            multipolygon = []
            for member in self.members:
                if member['role'] in {'outer', '', 'exclave'}:
                    if polygon:
                        # this polygon is finished
                        multipolygon.append(polygon)
                        polygon = []
                    try:
                        # append this way to the outer part of the polygon
                        outer = self.append_way(outer, member['ref'], polygon)
                    except (AttributeError, ValueError):
                        # attempt Nominatim geometry lookup
                        return self.nominatim_lookup()
                elif member['role'] in {'inner', 'enclave'}:
                    if not polygon:
                        # TODO: deal with lakes-with-islands, e.g. relation/3880511
                        return self.nominatim_lookup()
                    try:
                        # append this way to the inner part of the polygon
                        inner = self.append_way(inner, member['ref'], polygon)
                    except (AttributeError, ValueError):
                        return self.nominatim_lookup()
                elif member['role'] == 'admin_centre':
                    node = Node.collection.find_one({'_id': member['ref']})
                    if node:
                        self.tags.admin_centre = node.get_geometry()[1]
                elif member['role'] == 'label':
                    node = Node.collection.find_one({'_id': member['ref']})
                    if node:
                        self.tags.label = node.get_geometry()[1]

            if multipolygon:
                geo_type = "MultiPolygon"
                coordinates = multipolygon
            else:
                geo_type = "Polygon"
                coordinates = polygon

        elif rel_type in {'waterway', 'route', 'multilinestring', 'dual_carriageway'}:
            # first try stitching into a linestring
            # if endpoints don't match, make a multilinestring
            multilinestring = []
            linestring = []
            for member in self.members:
                if member['type'] == 'way':
                    try:
                        way = Way.collection.find_one({'_id': member['ref']})
                        w_gt, w_coords = way.get_geometry()
                    except AttributeError:
                        return self.nominatim_lookup()
                    try:
                        linestring, is_closed = stitch_linestrings(linestring, w_coords)
                    except ValueError:
                        multilinestring.append(linestring)
                        linestring = w_coords

            if multilinestring:
                geo_type = "MultiLineString"
                multilinestring.append(linestring)
                coordinates = multilinestring
            elif linestring:
                geo_type = "LineString"
                coordinates = linestring
            else:
                return self.nominatim_lookup()

        else:
            # ignore other types of relation
            return None, None

        return geo_type, coordinates

    @staticmethod
    def append_way(line_container, way_id, polygon):
        way = Way.collection.find_one({'_id': way_id})
        w_gt, w_coords = way.get_geometry()
        if w_gt == "Polygon":
            polygon.append(w_coords[0])
        else:
            # ensure that the linestring joins on a shared endpoint
            line_container, is_closed = stitch_linestrings(line_container, w_coords)
            if is_closed:
                # TODO: add exception for highway, railway, etc.
                polygon.append(line_container)
                line_container = []
        return line_container

    def nominatim_lookup(self):
        logger.info('Nominatim lookup for: relation/{}'.format(self._id))
        tdoc = Nominatim.reverse_and_query(self._id, 'R')
        if tdoc:
            return tdoc.get_geometry()
        else:
            return None, None


def stitch_linestrings(line_container, new_coordinates):
    if not line_container:
        combined_line = new_coordinates
    elif line_container[0] == new_coordinates[-1]:
        combined_line = new_coordinates + line_container[1:]
    elif line_container[-1] == new_coordinates[0]:
        combined_line = line_container + new_coordinates[1:]
    elif line_container[0] == new_coordinates[0]:
        combined_line = new_coordinates[::-1] + line_container[1:]
    elif line_container[-1] == new_coordinates[-1]:
        combined_line = line_container + new_coordinates[1::-1]
    else:
        raise ValueError(
            "Linestrings {} and {} don't share an endpoint.".format(line_container, new_coordinates)
        )

    return combined_line, line_is_closed(combined_line)


def line_is_closed(linestring):
    start_node = linestring[0]
    end_node = linestring[-1]
    return start_node == end_node


def osm_element_id(osm_instance):
    return '{}/{}'.format(osm_instance.collection.name, osm_instance._id)


class MalformedQuery(InvalidOperation):
    pass


class Feature(minimongo.Model):
    class Meta:
        collection = "gazetteer"
        indices = (
            minimongo.Index([
                ("properties.name", ASCENDING),
                ("properties.wikidata", ASCENDING),
                ("properties.wikipedia", ASCENDING),
                ("properties.pc_digits", ASCENDING),
            ], name='name_wiki_postal', sparse=True, unique=True),
            minimongo.Index([("provenance", ASCENDING)], name='provenance'),
            minimongo.Index([("geometry.type", ASCENDING)], name='geometry.type'),
            minimongo.Index([("geometry", GEOSPHERE)], name='geometry'),
            minimongo.Index([("known_names", ASCENDING)], name='known_names'),
            minimongo.Index([("municipalities", ASCENDING)], name='municipalities'),
            minimongo.Index([("districts", ASCENDING)], name='districts'),
            minimongo.Index([("neighborhoods", ASCENDING)], name='neighborhoods'),
        )

    def __init__(self, data=None):
        if data:
            super(Feature, self).__init__(data)
        else:
            self.type = "Feature"
            self.properties = {}
            self.geometry = {
                "type": "",
                "coordinates": []
            }
            self.provenance = []
            self.known_names = []
            self.llv_distance_cache = {}

    def get_property_keys(self):
        return self.properties.keys()

    @classmethod
    def create_or_update_from_osm(cls, osm_instance, merge_homonyms=False):

        existing_feat = None
        for property_key in ('wikidata', 'wikipedia', 'pc_digits'):
            if property_key in osm_instance.tags:
                existing_query = {
                    'properties.name': osm_instance.tags.name,
                    'properties.{}'.format(property_key): osm_instance.tags[property_key]
                }
                existing_feat = cls.collection.find_one(existing_query)
                if existing_feat:
                    break
        else:
            if merge_homonyms:
                existing_feat = cls.collection.find_one({
                    'properties.name': osm_instance.tags.name
                })
                if existing_feat:
                    logger.warning(u'Name-merging {}: {} into {}'.format(
                        osm_instance.tags.name,
                        osm_element_id(osm_instance),
                        existing_feat._id
                    ))

        if existing_feat:
            if osm_element_id(osm_instance) in existing_feat.provenance:
                logger.info('{} was found in {}'.format(
                    osm_element_id(osm_instance),
                    existing_feat._id
                ))
                return existing_feat
            else:
                feat = Feature(existing_feat.copy())
        else:
            feat = Feature()

        if isinstance(osm_instance, Node):
            position = [osm_instance.lon, osm_instance.lat]
            if (
                feat.geometry.type == 'Point'
                and feat.geometry.coordinates != position
            ):
                feat.geometry.type = 'MultiPoint'
                feat.geometry.coordinates = [
                    feat.geometry.coordinates, position
                ]
            elif (
                feat.geometry.type == 'MultiPoint'
                and position not in feat.geometry.coordinates
            ):
                feat.geometry.coordinates.append(position)
            elif feat.geometry.type:
                logger.info(u'Did not overwrite {} with {}'.format(feat, osm_instance))
            else:
                feat.geometry.type = 'Point'
                feat.geometry.coordinates = position

        elif isinstance(osm_instance, Way) or isinstance(osm_instance, Relation):
            geo_type, coordinates = osm_instance.get_geometry()
            new_geometry = {
                'type': geo_type,
                'coordinates': coordinates
            }
            if not geo_type:
                return None

            # check if feat.geometry should be replaced or combined
            if not feat.geometry.get('type'):
                feat.geometry = new_geometry
            elif (osm_instance.tags.get('admin_level')
                  and int(osm_instance.tags.admin_level) <= 3):
                # feature is a province or larger area
                # replace because comparison at this scale is a hassle
                feat.geometry = new_geometry
            elif feat.provenance and feat.contains_geometry(new_geometry):
                # the feature is good as it is
                logger.debug(feat.geometry.type, 'contains', geo_type)
            elif feat.provenance and feat.within_geometry(new_geometry):
                # replace the feature's geometry with the new geometry
                logger.debug(geo_type + 'contains' + feat.geometry.type)
                feat.geometry = new_geometry
            else:
                geometry_is_combined = False
                # try stitching linestrings together
                if feat.geometry.type == "LineString" and geo_type == "LineString":
                    try:
                        line_container = feat.geometry.coordinates
                        line_container, is_closed = stitch_linestrings(line_container, coordinates)
                        feat.geometry.coordinates = line_container
                        geometry_is_combined = True
                    except ValueError:
                        pass

                elif feat.geometry.type == "GeometryCollection":
                    # add the new geometry to the existing collection
                    feat.geometry.geometries.append(new_geometry)
                    geometry_is_combined = True

                elif feat.geometry.type in ('', 'Point'):
                    # existing geometry is empty or Node -> overwrite
                    feat.geometry = new_geometry
                    geometry_is_combined = True

                if not geometry_is_combined:
                    # combine the geometries in a collection
                    feat.geometry = {}
                    feat.geometry.type = "GeometryCollection"
                    feat.geometry.geometries = [existing_feat.geometry, new_geometry]

        else:
            raise TypeError(
                "{} is of the unexpected type {}".format(
                    osm_instance, type(osm_instance)
                )
            )

        # add names from existing feature
        if existing_feat:
            feat.get_known_names()

        # update properties from the OSM instance
        feat.properties.update(osm_instance.tags)
        feat.get_known_names()
        feat.provenance.append(
            osm_element_id(osm_instance)
        )
        try:
            return feat.save()
        except OperationFailure:
            logger.exception('Attempting to recover from MongoDB failure')
            nominatim_hit = feat.fetch_nominatim_details(set_geometry=True)
            if not nominatim_hit:
                if existing_feat:
                    feat = existing_feat
                else:
                    logger.warning(u'OSM instance appears obsolete: {}'.format(osm_instance))

            return feat.save()

    @classmethod
    def get_typology_dict(cls):
        res = cls.collection.aggregate([
            {
                '$group': {
                    '_id': '$properties.class',
                    'types': {'$addToSet': '$properties.type'}
                }
            }
        ])
        return {d['_id']: d['types'] for d in res['result']}

    def contains_geometry(self, other_geometry):
        return geometry_contains_other_geometry(self.geometry, other_geometry)

    def within_geometry(self, other_geometry):
        return geometry_contains_other_geometry(other_geometry, self.geometry)

    def get_known_names(self, language_codes=NL_LANGUAGES):
        name_set = set(getattr(self, 'known_names', [None]))
        # add (alternative) names in the national language
        for name_tag in OSM_NAME_TAGS:
            name_set.add(self.properties.get(name_tag))
        # add (alternative) internationalized names
        for lang_code in language_codes:
            for name_tag in OSM_NAME_TAGS:
                name_set.add(self.properties.get('{}:{}'.format(name_tag, lang_code)))

        name_set.discard(None)

        # prepend rail station names
        if 'railway:ref' in self.properties:
            station_prefix = u'Station '
            name_set = {
                name if name[1:].startswith(prefix[1:]) else prefix + name
                for name in name_set
                for prefix in (station_prefix, station_prefix.lower())
            }

        # exclude noncapital, single capital, and [^ANS]\d+
        re_bad_names = regex.compile(ur'^([^\p{Lu}\p{Lt}]+|[\p{Lu}\p{Lt}]|[^ANS]\d+)$')
        self.known_names = sorted([
            name
            for name in name_set
            if not re_bad_names.match(name)
        ])

        return self.known_names

    def fetch_nominatim_details(self, set_geometry=False, verbosity=0):
        # iterate through provenance: R>W>N
        for prov in sorted(self.provenance, key=sort_provenance_key):
            osm_type, osm_id = parse_provenance(prov)

            # Reverse Nominatim lookup and query by name
            tdoc = Nominatim.reverse_and_query(osm_id, osm_type)
            if tdoc:
                for key in ('display_name', 'importance', 'boundingbox',
                            'class', 'type', 'address', 'pc_digits'):
                    self.properties[key] = tdoc.get(key)

                if set_geometry:
                    geo_type, coordinates = tdoc.get_geometry()
                    self.geometry = {
                        'type': geo_type,
                        'coordinates': coordinates
                    }

                return tdoc

            elif verbosity:
                logger.warning(u'Could not find Nominatim details for {}'.format(prov))

    def get_simple_geotype(self):
        admin_level = self.properties.get('admin_level')
        osm_class = self.properties.get('class')
        osm_type = self.properties.get('type')

        if admin_level:
            return gt_map.by_admin_level(admin_level)
        elif osm_class or osm_type:
            return gt_map.by_class_and_type(osm_class, osm_type)
        else:
            return gt_map.by_other_properties(self.properties)

    def llv_distance(self, other):
        # check for cached distance
        distance = self.llv_distance_cache.get(str(other._id))

        # try the other feature's cache
        if distance is None:
            distance = other.llv_distance_cache.get(str(self._id))
            if distance is None:
                # compute the LLV distance
                distance = compute_llv_distance(self, other)

            self.llv_distance_cache[str(other._id)] = distance
            self.save()

        return distance

    # TODO: stitch lines within geometry collection


def sort_provenance_key(provenance_str):
    if provenance_str.startswith('mock_'):
        provenance_str = provenance_str[5:]

    if provenance_str.startswith('rel'):
        return 0
    elif provenance_str.startswith('way'):
        return 1
    elif provenance_str.startswith('node'):
        return 2
    else:
        raise ValueError('Unexpected provenance {}'.format(provenance_str))


def parse_provenance(provenance_str):
    osm_type, osm_id = provenance_str.split('/')
    if osm_type.startswith('mock_'):
        osm_type = osm_type[5:]
    return osm_type, osm_id


def geometry_contains_other_geometry(geometry_dict, other_geometry):
    if geometry_dict == other_geometry:
        return True

    if geometry_dict['type'] in ('Point', 'MultiPoint'):
        if other_geometry['type'] not in ('Point', 'MultiPoint'):
            return False
        return all(cp in flatten_coordinate_pairs(geometry_dict)
                   for cp in flatten_coordinate_pairs(other_geometry))

    elif geometry_dict['type'] in ('Polygon', 'MultiPolygon'):
        return polygon_contains_geometry(geometry_dict, other_geometry)

    elif geometry_dict['type'] == 'LineString':
        return linestring_contains_geometry(geometry_dict['coordinates'], other_geometry)

    elif geometry_dict['type'] == 'MultiLineString':
        if other_geometry['type'] == 'MultiLineString':
            for other_string in other_geometry['coordinates']:
                other_part = {'type': 'LineString', 'coordinates': other_string}
                if not any(
                    linestring_contains_geometry(line_string, other_part)
                    for line_string in geometry_dict['coordinates']
                ):
                    return False
            else:
                # all other parts are contained in the MultiLineString
                return True
        else:
            return any(
                linestring_contains_geometry(line_string, other_geometry)
                for line_string in geometry_dict['coordinates']
            )

    elif geometry_dict['type'] == 'GeometryCollection':
        return any(geometry_contains_other_geometry(gd, other_geometry)
                   for gd in geometry_dict['geometries'])
    else:
        raise MalformedQuery("This method must be called on a (Multi)Point, "
                             "-Polygon, -LineString, or GeometryCollection dict."
                             " Got '{}' instead.".format(geometry_dict['type']))


def polygon_contains_geometry(polygon_dict, other_geometry):
        # check if there is any geometry at all
        if not other_geometry.get('type'):
            return False
        # create a temporary feature from the other geometry
        temp_dict = {
            'geometry': other_geometry,
            'properties': {'name': 'temporary_feature', 'pc_digits': 'TEMP'},
            'known_names': []
        }
        # keep only the outer polygon(s)
        if polygon_dict['type'].startswith('Multi'):
            poly_coordinates = [poly[:1] for poly in polygon_dict['coordinates']]
        else:
            poly_coordinates = polygon_dict['coordinates'][:1]

        # prepare a geo-within query using only the outer polygon
        geo_within = {
            'geometry': {
                '$geoWithin': {
                    '$geometry': {
                        'type': polygon_dict['type'],
                        'coordinates': poly_coordinates
                    }
                }
            }
        }
        try:
            with temporary_feature(temp_dict) as tf:
                results = list(Feature.collection.find(geo_within))
                tf_in_results = (tf in results)
        except OperationFailure:
            tf_in_results = None

        if not tf_in_results:
            # workaround to detect parts of the polygon's border
            tf_in_results = all(cp in flatten_coordinate_pairs(polygon_dict)
                                for cp in flatten_coordinate_pairs(other_geometry))

        return tf_in_results


def linestring_contains_geometry(linestring_coords, other_geometry):
    if other_geometry['type'] in ('Point', 'MultiPoint'):
        return all(cp in linestring_coords for cp in flatten_coordinate_pairs(other_geometry))
    elif other_geometry['type'] == 'LineString':
        return (contains_subsequence(linestring_coords, other_geometry['coordinates'])
                or contains_subsequence(linestring_coords, other_geometry['coordinates'][::-1]))
    elif other_geometry['type'] == 'MultiLineString':
        return all(contains_subsequence(linestring_coords, coords)
                   or contains_subsequence(linestring_coords, coords[::-1])
                   for coords in other_geometry['coordinates'])
    else:
        # TODO: check geometry collections
        return False


def flatten_coordinate_pairs(geometry_dict):
        if geometry_dict['type'] == 'GeometryCollection':
            cp_list = []
            for geometry in geometry_dict['geometries']:
                cp_list += flatten_until_cp(geometry['coordinates'])
            return cp_list
        else:
            return flatten_until_cp(geometry_dict['coordinates'])


@contextmanager
def temporary_feature(data):
    temp_feature = Feature(data).save()
    yield temp_feature
    temp_feature.collection.delete_one({'_id': temp_feature._id})


def flatten_until_cp(l):
    if isinstance(l[0], float):
        yield l
    else:
        for el in l:
            if isinstance(el[0], float):
                yield el
            else:
                for sub in flatten_until_cp(el):
                    yield sub


class Nominatim(minimongo.Model):
    api_keys = []
    _key_iterator = None

    class Meta:
        collection = "nominatim_result"
        indices = (
            minimongo.Index([
                ("osm_id", ASCENDING),
                ("osm_type", ASCENDING)
            ], name='osm_id_type', unique=True),
            minimongo.Index("address.road"),
            minimongo.Index("display_name"),
            minimongo.Index([
                ('address.country', ASCENDING),
                ('address.state', ASCENDING),
                ('address.suburb', ASCENDING),
                ('address.postcode', ASCENDING),
                ('address.road', ASCENDING),
                ('address.city', ASCENDING),
                ('address.town', ASCENDING),
                ('address.county', ASCENDING),
                ('address.neighbourhood', ASCENDING),
                ('address.residential', ASCENDING),
                ('address.village', ASCENDING),
                ('address.city_district', ASCENDING),
                ('address.cycleway', ASCENDING),
                ('address.footway', ASCENDING),
                ('address.pedestrian', ASCENDING),
                ('address.path', ASCENDING)
            ], name='address_full')
        )

    @classmethod
    def mapquest_key(cls):
        keys_path = 'credentials/mapquest.keys'
        if not cls.api_keys:
            try:
                with open(keys_path) as f:
                    cls.api_keys = f.read().splitlines()

                cls._key_iterator = cycle(cls.api_keys)
            except IOError:
                logger.exception('Could not load Mapquest API keys')

        fresh_key = next(cls._key_iterator)
        if not fresh_key:
            raise CredentialsInvalid('No keys found in {}'.format(keys_path))
        return fresh_key

    @classmethod
    def get_res_by_address(cls, postcode=None, city=None, street=None):
        conditions = []
        query = {'$and': conditions}
        if postcode:
            query['address.postcode'] = postcode
        if city:
            conditions.append({'$or': [
                {'address.residential': city},
                {'address.suburb': city},
                {'address.village': city},
                {'address.town': city},
                {'address.city': city}
            ]})
        if street:
            conditions.append({'$or': [
                {'address.road': street},
                {'address.cycleway': street},
                {'address.footway': street},
                {'address.pedestrian': street},
                {'address.path': street}
            ]})
        return list(cls.collection.find(query))

    @classmethod
    @rate_limited(5)
    def query(cls, q=None, osm_id=None, attempt=0, **kwargs):
        url = "http://open.mapquestapi.com/nominatim/v1/{}.php".format(
            'reverse' if osm_id else 'search'
        )
        payload = {
            'format': 'json',
            'polygon_geojson': 1,
            'addressdetails': 1,
            'limit': 500,
            'key': cls.mapquest_key()
        }
        payload.update(kwargs)

        if q:
            payload['q'] = q
        elif osm_id:
            payload['osm_id'] = osm_id
            if len(payload['osm_type']) > 1:
                payload['osm_type'] = payload['osm_type'][0].upper()

        res = requests.get(url, params=payload)
        if res.status_code == 200:
            res_json = res.json()
            if 'error' in res_json:
                logger.warning(u"Nominatim error '{}', with payload: {}".format(
                    res_json['error'], payload
                ))
                return None
            return res.json()
        elif res.status_code == 403 and attempt < 5:
            # retry with different key
            return cls.query(q=q, osm_id=osm_id, attempt=1 + attempt, **kwargs)
        else:
            logger.error(res)
            return None

    @classmethod
    def reverse_and_query(cls, osm_id, osm_type):
        # Look for a cached result of this OSM instance
        tdoc = Nominatim.collection.find_one({'osm_type': osm_type, 'osm_id': osm_id})

        if not tdoc:
            # query Nominatim
            rres = cls.query(osm_id=osm_id, osm_type=osm_type)
            if rres:
                nres = cls.query(q=rres['display_name'])
                tdoc = cache_and_get_match(nres, osm_type, osm_id)
                if not tdoc:
                    nres = Nominatim.query(q=rres['display_name'].split(', ')[0])
                    tdoc = cache_and_get_match(nres, osm_type, osm_id)

        if tdoc:
            tdoc = Nominatim(tdoc)
            tdoc.name = tdoc.get_name_from_address()
            tdoc.pc_digits = tdoc.get_postcode_digits()

        return tdoc

    def to_mock_instance(self):
        data = {
            '_id': self.osm_id,
            'tags': {
                'display_name': self.display_name,
                'name': self.get_name_from_address(),
                'pc_digits': self.get_postcode_digits()
            }
        }
        if self.osm_type == 'node':
            data['lon'] = float(self.lon)
            data['lat'] = float(self.lat)
        else:
            data['geo_type'] = self.geojson.type
            data['coordinates'] = self.geojson.coordinates

        return mockmap[self.osm_type](data)

    def get_geometry(self):
        if self.osm_type == 'node':
            geo_type = "Point"
            coordinates = [float(self.lon), float(self.lat)]
        else:
            geo_type = self.geojson.type
            coordinates = self.geojson.coordinates

        return geo_type, coordinates

    def get_postcode_digits(self):
        try:
            return self.address.postcode[:4]
        except AttributeError:
            return

    def get_name_from_address(self):
        return (
            self.address.get('road')
            or self.address.get('cycleway')
            or self.address.get('footway')
            or self.address.get('pedestrian')
            or self.address.get('path')
        )


def cache_and_get_match(nominatim_results, osm_type, osm_id):
    for ndoc in nominatim_results:
        try:
            Nominatim(ndoc).save()
        except DuplicateKeyError:
            pass

        if ndoc.get('osm_type') == osm_type and ndoc.get('osm_id') == osm_id:
            return ndoc


classmap = {
    'node': Node,
    'way': Way,
    'relation': Relation
}


class MockNode(Node):
    def __init__(self, data):
        super(Node, self).__init__(data)
        self.lon = float(self.lon)
        self.lat = float(self.lat)

    @staticmethod
    def save():
        raise CannotSave("MockNode instances cannot be saved")


class MockWay(Way):
    def get_geometry(self):
        return self.geo_type, self.coordinates

    @staticmethod
    def save():
        raise CannotSave("MockWay instances cannot be saved")


class MockRelation(Relation):
    def get_geometry(self):
        return self.geo_type, self.coordinates

    @staticmethod
    def save():
        raise CannotSave("MockRelation instances cannot be saved")


mockmap = {
    'node': MockNode,
    'way': MockWay,
    'relation': MockRelation
}
