#!/usr/bin/env bash

change_port () {
    read -p "Please specify the desired port: " port
    echo
    if [[ $port =~ ^-?[0-9]+$ ]] ;
    then
        sed -i".bak" -e"s/port=8080/port=$port/g" "../run_server.py"
        echo "Server port was set to $port."
    else
        echo "$port is not a valid port. Try again."
        change_port
    fi
}

read -p "This will undo changes in your (local) working directory. Are you sure? (Y/N) " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    git checkout -- ../run_server.py
    echo "Git prompt/output"
    git pull
    rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi
    change_port
fi
