import re

from tqdm import tqdm

from loclinkvis.municipal.db_collections import (
    district_closeup,
    district_highover,
    municipality_closeup,
    municipality_highover,
    neighborhood_closeup,
    neighborhood_highover
)


def set_municipality_names():
    re_suffix = re.compile(r'(?P<name>.+)\s+\((?P<suffix>.+)\)')
    aliases = {
        "'s-Gravenhage": 'Den Haag',
        "'s-Hertogenbosch": 'Den Bosch',
    }

    for municipality in municipality_highover.find():
        cbs_code = municipality['properties']['GM_CODE']
        name = municipality['properties']['GM_NAAM']
        cbs_name = municipality['properties'].get('cbs_name') or name
        name = cbs_name
        known_names = [name]
        print(u'\nSetting names for {} {}...'.format(cbs_code, name))

        suffix_match = re_suffix.match(name)
        if suffix_match:
            known_names.append(suffix_match.group('name'))

        if name in aliases:
            known_names.append(aliases[name])

        if name[0].islower():
            known_names.append(name.title())
            name = name.title()

        field_updates = {
            'properties.GM_NAAM': name,
            'known_names': known_names,
            'properties.cbs_name': cbs_name
        }
        municipality_highover.update_one(
            {'_id': municipality['_id']},
            {'$set': field_updates}
        )
        municipality_closeup.update_one(
            {'properties.GM_CODE': cbs_code},
            {'$set': field_updates}
        )

        unique_toponyms = set_district_names(cbs_code, name)
        set_neighborhood_names(cbs_code, name, unique_toponyms)


def set_district_names(municipality_code, municipality_name):
    print('\nSetting district names...')
    re_suffix = re.compile(r'(?P<name>.+)\s+\((?P<suffix>.+)\)')
    re_wijk_pre = re.compile(r'(?P<wijk>Wijk \d\d)\s+(?P<name>.+)')
    re_wijk_post = re.compile(r'(?P<name>.+)\s+(?P<wijk>wijk \d\d)')

    unique_toponyms = set()
    in_municipality = {'properties.GM_CODE': municipality_code}
    with tqdm(total=district_highover.find(in_municipality).count()) as pbar:
        for district in district_highover.find(in_municipality).sort('properties.WK_NAAM', 1):
            pbar.update(1)
            name = district['properties']['WK_NAAM']
            cbs_name = district['properties'].get('cbs_name') or name
            name = cbs_name
            known_names = [name]

            wijk_pre_match = re_wijk_pre.match(name)
            if wijk_pre_match and wijk_pre_match.group('name') not in unique_toponyms:
                name = wijk_pre_match.group('name')
                known_names.append(name)
                known_names.append(wijk_pre_match.group('wijk'))

            wijk_post_match = re_wijk_post.match(name)
            if wijk_post_match and wijk_post_match.group('name') not in unique_toponyms:
                name = wijk_post_match.group('name')
                known_names.append(name)
                known_names.append(wijk_post_match.group('wijk').title())

            suffix_match = re_suffix.match(name)
            if suffix_match and suffix_match.group('name') not in unique_toponyms:
                known_names.append(suffix_match.group('name'))

            if name[0].islower():
                known_names.append(name.title())
                name = name.title()

            unique_toponyms.update(known_names)

            field_updates = {
                'properties.WK_NAAM': name,
                'properties.GM_NAAM': municipality_name,
                'known_names': known_names,
                'properties.cbs_name': cbs_name
            }
            district_highover.update_one(
                {'_id': district['_id']},
                {'$set': field_updates}
            )
            district_closeup.update_one(
                {'properties.WK_CODE': district['properties']['WK_CODE']},
                {'$set': field_updates}
            )

    return unique_toponyms


def set_neighborhood_names(municipality_code, municipality_name, district_names):
    print('\nSetting neighborhood names...')
    re_suffix = re.compile(r'(?P<name>.+)\s+\((?P<suffix>.+)\)')
    re_scattered = re.compile(r'Verspreide huizen (?P<qualifier>[a-z\s]+)?(?P<name>[\'st\s\-]*[A-Z].+)')

    unique_toponyms = set(district_names)
    in_municipality = {'properties.GM_CODE': municipality_code}
    with tqdm(total=neighborhood_highover.find(in_municipality).count()) as pbar:
        for neighborhood in neighborhood_highover.find(in_municipality).sort('properties.BU_NAAM', 1):
            pbar.update(1)
            name = neighborhood['properties']['BU_NAAM'].strip('"')
            cbs_name = neighborhood['properties'].get('cbs_name') or name
            name = cbs_name
            known_names = [name]

            suffix_match = re_suffix.match(name)
            if suffix_match and suffix_match.group('name') not in unique_toponyms:
                known_names.append(suffix_match.group('name'))

            scattered_match = re_scattered.match(name)
            if scattered_match:
                reference_name = scattered_match.group('name')
                qualifier = scattered_match.group('qualifier')
                shortened_name = u'V.h. {}'.format(reference_name)
                if qualifier:
                    qualified_name = u'V.h. {} {}'.format(qualifier.strip(), reference_name)
                else:
                    qualified_name = None

                if shortened_name not in unique_toponyms:
                    name = shortened_name
                    known_names.append(shortened_name)
                elif qualifier and qualified_name not in unique_toponyms:
                    name = qualified_name
                    known_names.append(qualified_name)

            if name[0].islower():
                known_names.append(name.title())
                name = name.title()

            unique_toponyms.update(known_names)

            field_updates = {
                'properties.BU_NAAM': name,
                'properties.GM_NAAM': municipality_name,
                'known_names': known_names,
                'properties.cbs_name': cbs_name
            }
            neighborhood_highover.update_one(
                {'_id': neighborhood['_id']},
                {'$set': field_updates}
            )
            neighborhood_closeup.update_one(
                {'properties.BU_CODE': neighborhood['properties']['BU_CODE']},
                {'$set': field_updates}
            )


def set_district_name_on_hoods():
    print('\nSetting district names on neighborhoods...')
    with tqdm(total=district_highover.count()) as pbar:
        for district in district_highover.find().batch_size(50):
            pbar.update(1)
            neighborhood_highover.update_many(
                {'properties.WK_CODE': district['properties']['WK_CODE']},
                {'$set': {
                    'properties.WK_NAAM': district['properties']['WK_NAAM'],
                }}
            )
            neighborhood_closeup.update_many(
                {'properties.WK_CODE': district['properties']['WK_CODE']},
                {'$set': {
                    'properties.WK_NAAM': district['properties']['WK_NAAM'],
                }}
            )


if __name__ == '__main__':
    set_municipality_names()
    set_district_name_on_hoods()

