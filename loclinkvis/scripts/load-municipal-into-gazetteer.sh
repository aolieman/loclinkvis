#!/bin/sh
SELECTOR='.features[] | select( .properties.WATER | contains("NEE") )'
jq -c "$SELECTOR" ../../cbs2017/gem_2017_highover.json | mongoimport --db osm_globe --collection=municipality_highover 2>&1 | tee ../../cbs2017/gem_2017_highover.log \
&& jq -c "$SELECTOR" ../../cbs2017/wijk_2017_highover.json | mongoimport --db osm_globe --collection=district_highover 2>&1 | tee ../../cbs2017/wijk_2017_highover.log \
&& jq -c "$SELECTOR" ../../cbs2017/buurt_2017_highover.json | mongoimport --db osm_globe --collection=neighborhood_highover 2>&1 | tee ../../cbs2017/buurt_2017_highover.log \
&& jq -c "$SELECTOR" ../../cbs2017/gem_2017_closeup.json | mongoimport --db osm_globe --collection=municipality_closeup 2>&1 | tee ../../cbs2017/gem_2017_closeup.log \
&& jq -c "$SELECTOR" ../../cbs2017/wijk_2017_closeup.json | mongoimport --db osm_globe --collection=district_closeup 2>&1 | tee ../../cbs2017/wijk_2017_closeup.log \
&& jq -c "$SELECTOR" ../../cbs2017/buurt_2017_closeup.json | mongoimport --db osm_globe --collection=neighborhood_closeup 2>&1 | tee ../../cbs2017/buurt_2017_closeup.log
