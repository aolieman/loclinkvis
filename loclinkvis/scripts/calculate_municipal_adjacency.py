from shapely.geometry import mapping, box, shape
from tqdm import tqdm

from loclinkvis.municipal.db_collections import (
    district_closeup,
    district_highover,
    municipality_closeup,
    municipality_highover,
    neighborhood_closeup,
    neighborhood_highover
)

MUNICIPALITY_BUFFER = 0.01
DISTRICT_BUFFER = 0.005
NEIGHBORHOOD_BUFFER = 0.001


def adjacency_query_factory(admin_boundary, buffer_degrees, extra_clauses=None):
    query = {
        'geometry': {
            '$geoIntersects': {
                '$geometry': mapping(
                    box(
                        *shape(admin_boundary['geometry']).buffer(buffer_degrees).bounds
                    )
                )
            }
        },
        '_id': {'$ne': admin_boundary['_id']}
    }
    if extra_clauses:
        query.update(**extra_clauses)
    return query


def update_municipality_adjacency():
    print 'Updating municipality adjacency'
    with tqdm(total=municipality_highover.count()) as pbar:
        for municipality in municipality_highover.find().batch_size(50):
            pbar.update(1)
            update_fields = {}

            # nearby municipalities, but not this one
            query = adjacency_query_factory(municipality, MUNICIPALITY_BUFFER)
            update_fields['adjacent_municipalities'] = [
                adjacent_municipality['properties']['GM_CODE']
                for adjacent_municipality in municipality_highover.find(query)
            ]

            update_fields = {
                '$set': update_fields
            }
            municipality_highover.update_one(
                {'_id': municipality['_id']},
                update_fields
            )
            municipality_closeup.update_one(
                {'properties.GM_CODE': municipality['properties']['GM_CODE']},
                update_fields
            )


def update_district_adjacency():
    print 'Updating district adjacency'
    with tqdm(total=district_highover.count()) as pbar:
        for district in district_highover.find().batch_size(50):
            pbar.update(1)
            update_fields = {}

            # nearby municipalities, but not the one the district is in
            municipality_query = adjacency_query_factory(district, MUNICIPALITY_BUFFER, {
                'properties.GM_CODE': {'$ne': district['properties']['GM_CODE']}
            })
            update_fields['adjacent_municipalities'] = [
                adjacent_municipality['properties']['GM_CODE']
                for adjacent_municipality in municipality_highover.find(municipality_query)
            ]

            # nearby districts in the same municipality, but not this one
            district_query = adjacency_query_factory(district, DISTRICT_BUFFER, {
                'properties.GM_CODE': district['properties']['GM_CODE']
            })
            update_fields['adjacent_districts'] = [
                adjacent_district['properties']['WK_CODE']
                for adjacent_district in district_highover.find(district_query)
            ]

            update_fields = {
                '$set': update_fields
            }

            district_highover.update_one(
                {'_id': district['_id']},
                update_fields
            )
            district_closeup.update_one(
                {'properties.WK_CODE': district['properties']['WK_CODE']},
                update_fields
            )


def update_neighborhood_adjacency():
    print 'Updating neighborhood adjacency'
    with tqdm(total=neighborhood_highover.count()) as pbar:
        for neighborhood in neighborhood_highover.find().batch_size(50):
            pbar.update(1)
            update_fields = {}

            # nearby municipalities, but not the one the neighborhood is in
            municipality_query = adjacency_query_factory(neighborhood, MUNICIPALITY_BUFFER, {
                'properties.GM_CODE': {'$ne': neighborhood['properties']['GM_CODE']}
            })
            update_fields['adjacent_municipalities'] = [
                adjacent_municipality['properties']['GM_CODE']
                for adjacent_municipality in municipality_highover.find(municipality_query)
            ]

            # nearby districts in the same municipality, but not the one the neighborhood is in
            district_query = adjacency_query_factory(neighborhood, DISTRICT_BUFFER, {
                '$and': [
                    {'properties.GM_CODE': neighborhood['properties']['GM_CODE']},
                    {'properties.WK_CODE': {'$ne': neighborhood['properties']['WK_CODE']}}
                ]
            })
            update_fields['adjacent_districts'] = [
                adjacent_district['properties']['WK_CODE']
                for adjacent_district in district_highover.find(district_query)
            ]

            # nearby neighborhoods in the same district, but not this one
            neighborhood_query = adjacency_query_factory(neighborhood, NEIGHBORHOOD_BUFFER, {
                'properties.WK_CODE': neighborhood['properties']['WK_CODE']
            })
            update_fields['adjacent_neighborhoods'] = [
                adjacent_neighborhood['properties']['BU_CODE']
                for adjacent_neighborhood in neighborhood_highover.find(neighborhood_query)
            ]

            update_fields = {
                '$set': update_fields
            }
            neighborhood_highover.update_one(
                {'_id': neighborhood['_id']},
                update_fields
            )
            neighborhood_closeup.update_one(
                {'properties.BU_CODE': neighborhood['properties']['BU_CODE']},
                update_fields
            )


if __name__ == '__main__':
    update_municipality_adjacency()
    update_district_adjacency()
    update_neighborhood_adjacency()
