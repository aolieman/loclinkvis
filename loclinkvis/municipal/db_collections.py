import pymongo
from pymongo import IndexModel, ASCENDING, GEOSPHERE
from pymongo.errors import ServerSelectionTimeoutError

from loclinkvis.exceptions import DbConnectionError
from loclinkvis.mongocredentials import MONGODB_HOST, MONGODB_PORT, MONGODB_DATABASE
from loclinkvis.web_app import app


client = pymongo.MongoClient(
    MONGODB_HOST,
    MONGODB_PORT,
    serverSelectionTimeoutMS=int(10e3)
)
db = client[MONGODB_DATABASE]
try:
    app.logger.info('Established MongoDB connection with {}:{}'.format(*client.address))
except ServerSelectionTimeoutError:
    raise DbConnectionError(
        'Could not connect to MongoDB server at {}:{}'.format(MONGODB_HOST, MONGODB_PORT)
    )


def index_factory(id_prefix, *parent_prefixes):
    index_definitions = [
        IndexModel([
            ('properties.' + id_prefix + '_CODE', ASCENDING)
        ], name='cbs_id_code', unique=True),
        IndexModel([
            ('properties.' + prefix + '_NAAM', ASCENDING)
            for prefix in (id_prefix,) + parent_prefixes
        ], name='cbs_names'),
        IndexModel([("geometry.type", ASCENDING)], name='geometry.type'),
        IndexModel([("geometry", GEOSPHERE)], name='geometry'),
    ]

    if parent_prefixes:
        index_definitions.append(
            IndexModel([
                ('properties.' + prefix + '_CODE', ASCENDING)
                for prefix in parent_prefixes
            ], name='cbs_parent_codes')
        )

    return index_definitions


municipality_indexes = index_factory('GM')
municipality_highover = db['municipality_highover']
municipality_highover.create_indexes(municipality_indexes)
municipality_closeup = db['municipality_closeup']
municipality_closeup.create_indexes(municipality_indexes)

district_indexes = index_factory('WK', 'GM')
district_highover = db['district_highover']
district_highover.create_indexes(district_indexes)
district_closeup = db['district_closeup']
district_closeup.create_indexes(district_indexes)

neighborhood_indexes = index_factory('BU', 'WK', 'GM')
neighborhood_highover = db['neighborhood_highover']
neighborhood_highover.create_indexes(neighborhood_indexes)
neighborhood_closeup = db['neighborhood_closeup']
neighborhood_closeup.create_indexes(neighborhood_indexes)
