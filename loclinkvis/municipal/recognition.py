from cachetools import LRUCache

from loclinkvis.gazetteer.models import Feature
from loclinkvis.geocoding.location_name_tree import LocationNameSearcher, get_toponym_postings
from loclinkvis.municipal import db_collections


def build_municipal_searcher(cbs_code):
    # get municipal toponyms from gazetteer
    toponyms = Feature.collection.distinct(
        'known_names',
        {'municipalities': cbs_code}
    )
    # add names of districts and neighborhoods
    toponyms += db_collections.district_highover.distinct(
        'known_names',
        {'properties.GM_CODE': cbs_code}
    )
    toponyms += db_collections.neighborhood_highover.distinct(
        'known_names',
        {'properties.GM_CODE': cbs_code}
    )
    return LocationNameSearcher(toponyms)


_searcher_cache = LRUCache(maxsize=32, missing=build_municipal_searcher)


def get_municipal_searcher(cbs_code):
    return _searcher_cache[cbs_code]


def spot_toponyms_in_text(cbs_code, text):
    searcher = get_municipal_searcher(cbs_code)
    valid, faux = searcher.spot_toponyms(text, details=True)
    valid_postings = get_toponym_postings(valid)
    faux_postings = get_toponym_postings(faux)

    return valid_postings, faux_postings
