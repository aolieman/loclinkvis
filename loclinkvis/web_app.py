import os
from logging.config import dictConfig

from flask import Flask
from flask_cors import CORS


# Configure logging
project_dir = os.path.dirname(
    os.path.dirname(
        os.path.realpath(__file__)
    )
)
log_filepath = os.path.join(project_dir, 'logs', 'webserver.log')

dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(levelname)s in %(name)-12s: %(message)s'
        },
        'timed': {
            'format': '[%(asctime)s] %(levelname)s in %(name)-12s: %(message)s',
            'datefmt': '%m-%d %H:%M'
        }
    },
    'handlers': {
        'stream': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'timed',
            'filename': log_filepath,
            'mode': 'a',
            'maxBytes': 67000,
            'backupCount': 5,
        },
    },
    'root': {
        'level': 'INFO',
        'handlers': ['stream', 'file']
    }
})

# Initialize a Flask instance
app = Flask('loclinkvis', static_folder='geo_demo/static')
app.url_map.strict_slashes = False

# Cross-origin
CORS(app)
