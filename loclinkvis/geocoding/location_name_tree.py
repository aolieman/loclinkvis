# -*- coding: utf-8 -*-
"""
Build a location name tree based on the gazetteer.

"""
import logging
import re
from collections import defaultdict
from itertools import groupby
from operator import itemgetter

from acora import AcoraBuilder

from loclinkvis.exceptions import NameSearcherError
from not_a_toponym_detection import FauxToponymDetector


logger = logging.getLogger(__name__)


class LocationNameSearcher(object):
    """A queryable interface for location names that are
       present in a gazetteer based on OSM data.
    """

    def __init__(self, names):
        if not names:
            raise NameSearcherError(
                'A container with at least one string needs to be provided '
                'instead of: {}'.format(repr(names))
            )
        try:
            logger.info('Building a tree with {} names...'.format(len(names)))
        except TypeError:
            logger.info('Building a name tree from generator...')
        builder = AcoraBuilder(*filter(None, names))
        logger.info('  Converting the tree to its searchable form')
        self.name_tree = builder.build()

        logger.info('Initializing a faux-toponym detector...')
        self.ftd = FauxToponymDetector()

    def find_in_tree(self, s):
        acora_gen = self.name_tree.finditer(s)
        for kw, pos in self.longest_match(acora_gen):
            name_span = (pos, pos + len(kw))
            context_span = ((name_span[0] or 1) - 1, name_span[1] + 6)
            name_candidate = s[slice(*name_span)]
            exp_candidate = s[slice(*context_span)]

            # Allowed suffixes: -s(e), -(m)er(s), -(e)naar(s)
            name_boundary = re.compile(
                u'{}(s(e)?|(m)?er(s)?|(e)?naar(s)?)?\\b'.format(re.escape(name_candidate)),
                re.UNICODE
            )
            re_match = re.search(name_boundary, exp_candidate)
            if re_match:
                yield (name_span, name_candidate)

    @staticmethod
    def longest_match(matches):
        for pos, match_set in groupby(matches, itemgetter(1)):
            yield max(match_set)

    def spot_toponyms(self, text, details=False):
        spotted_matches = self.find_in_tree(text)
        if details:
            return self.ftd.split_valid_and_faux_matches(spotted_matches)
        else:
            return self.ftd.exclude_faux_matches(spotted_matches)


def get_toponym_postings(matches):
    postings = defaultdict(list)
    for name_span, name_candidate in matches:
        postings[name_candidate].append(name_span)

    return dict(postings)
