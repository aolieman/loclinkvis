import logging
import re
import json
from collections import defaultdict

import numpy
from sklearn.feature_extraction import DictVectorizer
from sklearn.metrics import classification_report
from sklearn import svm
from sklearn.externals import joblib
from SPARQLWrapper import SPARQLWrapper, JSON

from loclinkvis.gazetteer.models import Feature
from loclinkvis.settings import NL_NAT_DETECTION as NATs
from loclinkvis.utils.web import rate_limited

logger = logging.getLogger(__name__)


class FauxToponymDetector(object):

    def __init__(self):
        with open('../statistical_models/training/topo_in_wiktionary.json', 'rb') as f:
            faux_toponyms = {k for k, v in json.load(f)}
        with open('../statistical_models/place_in_nl.json', 'rb') as f:
            true_toponyms = set(json.load(f))

        self.faux_toponyms = faux_toponyms - true_toponyms

    def is_faux_toponym(self, t):
        return t in self.faux_toponyms

    def exclude_faux_matches(self, toponym_matches):
        for name_span, name_candidate in toponym_matches:
            if not self.is_faux_toponym(name_candidate):
                yield (name_span, name_candidate)

    def split_valid_and_faux_matches(self, toponym_matches):
        valid_matches, faux_matches = [], []
        for match in toponym_matches:
            if self.is_faux_toponym(match[1]):
                faux_matches.append(match)
            else:
                valid_matches.append(match)

        return valid_matches, faux_matches


def prune_names():
    # TODO issues: Vlaanderen, Koninkrijk der Nederlanden
    # read a list of possible toponyms in wiktionary
    with open('../statistical_models/training/topo_in_wiktionary.json', 'rb') as f:
        in_wiktionary = {k: v for k, v in json.load(f)}

    sparql = SPARQLWrapper("http://nl.dbpedia.org/sparql")
    sparql.setReturnFormat(JSON)

    not_a_location = []
    possible_place_in_nl = {}
    place_elsewhere = {}

    lon_range = NATs['lon_range']
    lat_range = NATs['lat_range']

    neighbors = NATs['neighbors']

    # decide for each possible toponym, whether:
    for toponym, frequency in in_wiktionary.iteritems():
        logger.info(toponym)
        res = toponym_sparql_query(toponym, sparql)
        has_been_classified = False

        if len(res) == 0:
            # the name doesn't refer to a known place
            not_a_location.append((toponym, frequency))
            has_been_classified = True
        elif not res_within_bbox(res, lon_range, lat_range):
            # coordinates are outside of NL or unknown
            if (
                any(r.get('pc') and 1000 <= r['pc'] <= 9999 for r in res.values())
                # toponym has an associated postal code that may be Dutch
                and not any((
                    res_outside_bbox(res, lon_range, lat_range),
                    any(any(nb in uri for nb in neighbors) for uri in uris_from_res(res))
                ))
            ):
                # d does not refer to a neigboring state,
                #   nor does it refer to coordinates outside the NL bbox
                pass
            else:
                place_elsewhere[toponym] = (frequency, res)
                has_been_classified = True

        if not has_been_classified:
            # the place is located within the NL bbox
            #  or lacks indicators that it is located in another country
            possible_place_in_nl[toponym] = (frequency, exclude_non_national(res))

    # Classify the most difficult cases
    not_often_in_nl, place_in_nl, clf = classify_possible_topo_in_nl(possible_place_in_nl)

    with open('../statistical_models/not_a_location.json', 'wb') as f:
        json.dump(not_a_location, f)
    with open('../statistical_models/not_often_in_nl.json', 'wb') as f:
        json.dump(not_often_in_nl, f)
    with open('../statistical_models/place_elsewhere.json', 'wb') as f:
        json.dump(place_elsewhere, f)
    with open('../statistical_models/place_in_nl.json', 'wb') as f:
        json.dump(place_in_nl, f)

    return not_a_location, not_often_in_nl, place_elsewhere, place_in_nl


def uris_from_res(sparql_result):
    uris = []
    for item in sparql_result.items():
        uris += uris_from_ri(item)
    return uris


def uris_from_ri(res_item):
    uris = [res_item[0]]
    for key in ('ic', 'lc'):
        if res_item[1].get(key):
            uris.append(res_item[1][key])

    return uris + res_item[1]['categories']


def rb_refers_to_nation(res_binding, nation):
    return (
        (res_binding.get('ic') and res_binding['ic'].endswith(nation))
        or (res_binding.get('lc') and res_binding['lc'].endswith(nation))
    )


def res_within_bbox(sparql_result, lon_range, lat_range):
    # returns true if any coordinates fall within or on the bbox
    nation = NATs['nation_name']
    return any(
        (
            (lon_range[0] <= p.get('lon', 0) <= lon_range[1])
            and (lat_range[0] <= p.get('lat', 0) <= lat_range[1])
        )
        or rb_refers_to_nation(p, nation)
        for p in sparql_result.values()
    )


def res_outside_bbox(sparql_result, lon_range, lat_range):
    # returns true if any coordinates fall outside of the bbox
    return any(
        res_binding_outside_bbox(rb, lon_range, lat_range)
        for rb in sparql_result.values()
    )


def res_binding_outside_bbox(rb, lon_range, lat_range):
    if rb.get('lon') and not (lon_range[0] <= rb['lon'] <= lon_range[1]):
        return True
    if rb.get('lat') and not (lat_range[0] <= rb['lat'] <= lat_range[1]):
        return True

    return False


@rate_limited(10)
def toponym_sparql_query(toponym, sparql_wrapper=None):

    if not sparql_wrapper:
        sparql_wrapper = SPARQLWrapper("http://nl.dbpedia.org/sparql")
        sparql_wrapper.setReturnFormat(JSON)

    sparql_wrapper.setQuery("""
        SELECT DISTINCT ?place ?category ?pl ?pod ?lon ?lat ?ic ?lc ?pn ?st ?pc
        WHERE {{
            ?place prop-nl:naam "{}"@nl .
            ?place a dbpedia-owl:Place .
            ?place dcterms:subject ?category .
            ?place dbpedia-owl:wikiPageLength ?pl.
            ?place dbpedia-owl:wikiPageOutDegree ?pod.
            OPTIONAL {{
                ?place prop-nl:lonDeg ?lon .
                ?place prop-nl:latDeg ?lat .
            }}
            OPTIONAL {{ ?place dbpedia-owl:country ?ic . }}
            OPTIONAL {{ ?place prop-nl:land ?lc . }}
            OPTIONAL {{ ?place prop-nl:inwoners ?pn . }}
            OPTIONAL {{ ?place prop-nl:soort ?st . }}
            OPTIONAL {{ ?place prop-nl:postcode ?pc . }}
        }} LIMIT 100
    """.format(toponym))
    results = sparql_wrapper.query().convert()
    joined_res = defaultdict(sparql_result_factory)

    for res in results['results']['bindings']:
        place = res['place']['value']
        joined_res[place]['categories'].append(res['category']['value'])
        for int_key in ('lon', 'lat', 'pn', 'pc', 'pl', 'pod'):
            if res.get(int_key):
                try:
                    joined_res[place][int_key] = int(re.sub(r'\D', '', res[int_key]['value']))
                except ValueError:
                    joined_res[place][int_key] = 0
        for str_key in ('ic', 'lc', 'st'):
            if res.get(str_key):
                joined_res[place][str_key] = res[str_key]['value']

    return joined_res


def sparql_result_factory():
    return {'categories': []}


def get_property_stats(toponym):
    features = Feature.collection.find({'known_names': toponym})
    key_counts = [len(f.get_property_keys()) for f in features]
    return simple_descriptives(key_counts)


def simple_descriptives(numeric_iterator):
    if not numeric_iterator:
        return None
    return min(numeric_iterator), numpy.mean(numeric_iterator), max(numeric_iterator)


def starts_with_cardinal_direction(toponym):
    return any(
        toponym.lower().startswith(car_dir)
        for car_dir in NATs['cardinal_directions']
    )


def ends_with_cardinal_direction(toponym):
    return any(
        toponym.lower().endswith(car_dir)
        for car_dir in NATs['cardinal_directions']
    )


def exclude_non_national(sparql_result):
    return {
        k: v
        for k, v in sparql_result.items()
        if not any((
            (v.get('pc') and not 1000 <= v['pc'] <= 9999),
            any(any(nb in uri for nb in NATs['neighbors']) for uri in uris_from_ri((k, v))),
            res_binding_outside_bbox(v, NATs['lon_range'], NATs['lat_range'])
        ))
    }


def descriptives_for_key(sparql_res, key):
    key_vals = [vals[key] for vals in sparql_res.values() if vals.get(key)]
    return simple_descriptives(key_vals)


def wf_per_num(wf, sparql_res, key):
    descriptives = descriptives_for_key(sparql_res, key)
    avg_num = descriptives[1] if descriptives else (1 / float(wf))
    return wf / avg_num


def wf_per_pstats(toponym, wf):
    return wf / get_property_stats(toponym)[1]


def gather_classifier_input(possible_pins=None):

    with open('../statistical_models/training/not_often_nl_place_examples.json', 'rb') as f:
        non_pes = json.load(f)

    with open('../statistical_models/training/place_in_netherlands_examples.json', 'rb') as f:
        pin_es = json.load(f)

    if not possible_pins:
        with open('../statistical_models/training/possible_place_in_nl.json', 'rb') as f:
            possible_pins = json.load(f)

    training_tuples = []
    logger.info("Loading {} negative training examples".format(len(non_pes)))
    for toponym in non_pes:
        wf, res = possible_pins[toponym]
        training_tuples.append((toponym, 0, make_feature_dict(toponym, wf, res)))
        logger.info("Loading {} positive training examples".format(len(pin_es)))
    for toponym in pin_es:
        wf, res = possible_pins[toponym]
        training_tuples.append((toponym, 1, make_feature_dict(toponym, wf, res)))

        logger.info("Loading {} toponyms to classify".format(len(possible_pins)))
    all_tuples = []
    for toponym, wf_res in possible_pins.iteritems():
        wf, res = wf_res
        all_tuples.append((toponym, make_feature_dict(toponym, wf, res)))

    return training_tuples, all_tuples


def make_feature_dict(toponym, wf, sparql_res):

    min_tag_stats, avg_tag_stats, max_tag_stats = get_property_stats(toponym)

    feat_dict = {
        'wiktionary_frequency': wf,
        'starts_with_car_dir': starts_with_cardinal_direction(toponym),
        'ends_with_car_dir': ends_with_cardinal_direction(toponym),
        'min_tag_stats': min_tag_stats,
        'avg_tag_stats': avg_tag_stats,
        'max_tag_stats': max_tag_stats,
        'wf_per_pstats': wf_per_pstats(toponym, wf),
        'wf_per_wpl': wf_per_num(wf, sparql_res, 'pl'),
        'wf_per_wpod': wf_per_num(wf, sparql_res, 'pod')
    }

    for name, key in (('population', 'pn'), ('page_length', 'pl'), ('page_outdegree', 'pod')):
        descriptives = descriptives_for_key(sparql_res, key)
        if descriptives:
            num_min, num_avg, num_max = descriptives
            feat_dict['{}_min'.format(name)] = num_min
            feat_dict['{}_avg'.format(name)] = num_avg
            feat_dict['{}_max'.format(name)] = num_max

    place_types = {
        vals['st'].lower().strip()
        for vals in sparql_res.values() if vals.get('st')
    }
    for place_type in place_types:
        feat_dict['place={}'.format(place_type)] = True

    return feat_dict


def classify_possible_topo_in_nl(possible_pins=None):

    vec = DictVectorizer()
    training_tuples, all_tuples = gather_classifier_input(possible_pins)
    names, classes, dicts = zip(*training_tuples)
    all_names, all_dicts = zip(*all_tuples)

    matrix = vec.fit_transform(dicts)
    logger.info("Training on features:\n{}".format(vec.get_feature_names()))
    clf = svm.SVC()
    clf.fit(matrix, classes)
    pred = clf.predict(matrix)
    target_names = ['not_often_in_nl', 'place_in_nl']
    logger.info("\nSelf-test: performance on 100% of training data")
    logger.info(classification_report(classes, pred, target_names=target_names))

    # Classify all toponyms that are possibly used as such in Dutch
    all_matrix = vec.transform(all_dicts)
    all_pred = clf.predict(all_matrix)
    not_often_in_nl, place_in_nl = [], []
    for i, class_int in enumerate(all_pred):
        if class_int == 0:
            not_often_in_nl.append(all_names[i])
        elif class_int == 1:
            place_in_nl.append(all_names[i])
        else:
            raise ValueError('Predicted unexpected class {} for {}'.format(class_int, all_matrix[i]))

    if not possible_pins:
        with open('../statistical_models/not_often_in_nl.json', 'wb') as f:
            json.dump(not_often_in_nl, f, indent=4)
        with open('../statistical_models/place_in_nl.json', 'wb') as f:
            json.dump(place_in_nl, f, indent=4)

    joblib.dump(clf, '../statistical_models/possible_place_in_nl.clf')

    return not_often_in_nl, place_in_nl, clf
