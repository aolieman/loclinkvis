import networkx
from shapely.geometry import shape
from numpy import mean
from collections import defaultdict
from loclinkvis.settings import NL_SPATIAL_ANALYSIS as SA


def as_shapes(feature):
    if feature.geometry.type.lower() == 'geometrycollection':
        for geom in feature.geometry.geometries:
            yield shape(geom)
    else:
        yield shape(feature.geometry)


def compute_llv_distance(feature1, feature2):
    distances = []

    for ashp in as_shapes(feature1):
        for rshp in as_shapes(feature2):
            if ashp.covers(rshp) or rshp.covers(ashp):
                distances.append(1)
            elif ashp.intersects(rshp):
                distances.append(10)
            else:
                dist_in_km = ashp.distance(rshp) * SA['line_multiplier']
                distances.append(10 + dist_in_km)

    return mean(distances)


def llv_distance_ranking(features_per_toponym, personalize=True):
    all_features = []
    ambiguity_dict = defaultdict(list)

    for tnym, candidates in features_per_toponym.iteritems():
        for feat in candidates:
            if feat not in all_features:
                all_features.append(feat)
            ambiguity_dict[str(feat._id)].append(1.0 / len(candidates))

    personalization_dict = {k: mean(v) for k, v in ambiguity_dict.iteritems()}

    # add nodes and edges to a disambiguation graph
    llv_g = networkx.Graph()

    while len(all_features) > 0:
        feat = all_features.pop()

        for other in all_features:
            llv_similarity = 1.0 / feat.llv_distance(other)
            llv_g.add_edge(str(feat._id), str(other._id), weight=llv_similarity)

    # compute ranking with (personalized) weighted PageRank
    p13n_dict = personalization_dict if personalize else None
    llv_dist_ranking = networkx.pagerank_numpy(llv_g, personalization=p13n_dict, weight='weight')

    return llv_dist_ranking


def test_principles():
    from loclinkvis.gazetteer.models import Feature

    ams = Feature.collection.find_one({'known_names': 'Amsterdam'})
    rtm = Feature.collection.find_one({'known_names': 'Rotterdam'})
    dft = Feature.collection.find_one({'known_names': 'Delft'})
    csl = Feature.collection.find_one({'known_names': 'Coolsingel'})

    for i, ashp in enumerate(as_shapes(ams)):
        for j, rshp in enumerate(as_shapes(rtm)):
            distance = ashp.distance(rshp) * 142
            print 'Distance between Amsterdam {} and Rotterdam {}: {} '.format(i, j, distance)

    for i, rshp in enumerate(as_shapes(rtm)):
        for j, dshp in enumerate(as_shapes(dft)):
            distance = rshp.distance(dshp) * 142
            print 'Distance between Rotterdam {} and Delft {}: {} '.format(i, j, distance)

    print 'Relations between Amsterdam and Rotterdam: {}'.format(as_shapes(ams).next().relate(as_shapes(rtm).next()))
    print 'Relations between Rotterdam and Delft: {}'.format(as_shapes(rtm).next().relate(as_shapes(dft).next()))
    print 'Relations between Rotterdam and Coolsingel: {}'.format(as_shapes(rtm).next().relate(as_shapes(csl).next()))

    print 'Spatial footprint of Rotterdam: {}'.format(as_shapes(rtm).next().area * 7630)

    topos = [
        'Delft', 'Nederland', 'Rijswijk', 'Zuiderstraat', 'Vlamingstraat',
        'Ceintuurbaan', 'Emmastraat', 'Meeslaan', 'Haagweg', 'Singel', 'Nieuwe Kerk'
    ]
    feats_per_topo = {t: list(Feature.collection.find({'known_names': t})) for t in topos}

    return rank_info(llv_distance_ranking(feats_per_topo, personalize=True))


def rank_info(llv_dist_ranking):
    from loclinkvis.gazetteer.models import Feature
    from bson import ObjectId

    sort_ranking = sorted(llv_dist_ranking.items(), key=lambda t: t[1], reverse=True)

    for pt in sort_ranking:
        feat = Feature.collection.find_one({'_id': ObjectId(pt[0])})
        print pt[1], feat.get_simple_geotype(), feat.known_names, feat.properties.get('address')
