import os

MONGODB_HOST = os.environ.get('MONGODB_HOST', 'localhost')
MONGODB_PORT = os.environ.get('MONGODB_PORT', 27017)
MONGODB_DATABASE = os.environ.get('MONGODB_DATABASE', 'osm_globe')
