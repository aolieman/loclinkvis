#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Run a Flask server .
"""

from loclinkvis import settings
from loclinkvis.web_app import app

# Flask views
views_loaded = False
if settings.SERVE_GEO_DEMO is True:
    import loclinkvis.geo_demo.views
    views_loaded = True

if settings.SERVE_MUNICIPAL is True:
    import loclinkvis.municipal.views
    views_loaded = True

if not views_loaded:
    app.logger.warning(
        'The webserver is being run without any loaded views. '
        'Make sure that at least one SERVE_* setting is True.'
    )


if __name__ == '__main__':
    app.logger.info([
        (rule.rule, rule._regex.pattern)
        for rule in app.url_map.iter_rules()
        if rule.endpoint != 'static'
    ])
    app.run(host='0.0.0.0', port=8080)
