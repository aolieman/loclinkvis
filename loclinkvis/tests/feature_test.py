from unittest import TestCase

from pymongo import ASCENDING, GEOSPHERE
import minimongo

from loclinkvis.gazetteer.models import geometry_contains_other_geometry


class DisposableGeometry(minimongo.Model):
    class Meta:
        collection = "test_geometry"
        indices = (
            minimongo.Index([("name", ASCENDING)], name='name', unique=True),
            minimongo.Index([("geometry", GEOSPHERE)], name='geometry'),
        )

    def __init__(self, data=None):
        if data:
            super(DisposableGeometry, self).__init__(data)
        else:
            self.type = "some kind of test"
            self.geometry = {
                "type": "",
                "name": "",
                "coordinates": []
            }


class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self


class TestGeometryContainment(TestCase):

    gj = AttrDict()

    @classmethod
    def setup_class(cls):
        # basic geometry
        origin = [4.895691, 52.369029]
        position_a = origin
        position_b = [origin[0] + 1.5, origin[1] + 0.5]
        r = 0.2
        triangle_a = [[
            [position_a[0], position_a[1] + r],
            [position_a[0] + r, position_a[1] - r],
            [position_a[0] - r, position_a[1] - r],
            [position_a[0], position_a[1] + r]
        ]]
        triangle_b = [[
            [position_b[0], position_b[1] - r],
            [position_b[0] - r, position_b[1] + r],
            [position_b[0] + r, position_b[1] + r],
            [position_b[0], position_b[1] - r]
        ]]
        line_a = [position_a, [origin[0] + 1, origin[1] + 1]]
        line_b = line_a + [[origin[0] + 2, origin[1] + 1]]
        line_c = line_b[1:]
        line_d = [[origin[0] + 2, origin[1]], [origin[0] + 1, origin[1]]]
        square = [line_c + line_d + line_c[:1], triangle_b[0]]

        # geojson geometry
        cls.gj.point_a = {
            "type": "Point",
            "coordinates": position_a
        }
        cls.gj.point_b = {
            "type": "Point",
            "coordinates": position_b
        }
        cls.gj.multipoint = {
            "type": "MultiPoint",
            "coordinates": [position_a, position_b]
        }
        cls.gj.polygon_a = {
            "type": "Polygon",
            "coordinates": triangle_a
        }
        cls.gj.polygon_b = {
            "type": "Polygon",
            "coordinates": triangle_b
        }
        cls.gj.multipolygon = {
            "type": "MultiPolygon",
            "coordinates": [triangle_a, triangle_b]
        }
        cls.gj.linestring_a = {
            "type": "LineString",
            "coordinates": line_a
        }
        cls.gj.linestring_b = {
            "type": "LineString",
            "coordinates": line_b
        }
        cls.gj.linestring_d = {
            "type": "LineString",
            "coordinates": line_d
        }
        cls.gj.multilinestring = {
            "type": "MultiLineString",
            "coordinates": [line_a, line_c]
        }
        cls.gj.polygon_c = {
            "type": "Polygon",
            "coordinates": square
        }
        cls.gj.geo_collection = {
            "type": "GeometryCollection",
            "geometries": [
                {
                    "type": "Polygon",
                    "coordinates": triangle_b
                },
                {
                    "type": "LineString",
                    "coordinates": line_d
                }
            ]
        }

        # create mongodb documents
        for name, geo_dict in cls.gj.items():
            DisposableGeometry({
                'type': 'geometry containment',
                'name': name,
                'geometry': geo_dict
            }).save()

    @classmethod
    def teardown_class(cls):
        DisposableGeometry.collection.drop()

    # test methods
    # TODO: the following can be generated from a containment hierarchy
    def test_point_in_self(self):
        b = geometry_contains_other_geometry(self.gj.point_a, self.gj.point_a)
        self.assertTrue(b)

    def test_point_in_line(self):
        b = geometry_contains_other_geometry(self.gj.linestring_a, self.gj.point_a)
        self.assertTrue(b)

    def test_line_in_point(self):
        b = geometry_contains_other_geometry(self.gj.point_a, self.gj.linestring_a)
        self.assertFalse(b)

    def test_line_in_self(self):
        b = geometry_contains_other_geometry(self.gj.linestring_a, self.gj.linestring_a)
        self.assertTrue(b)

    def test_line_in_line(self):
        b = geometry_contains_other_geometry(self.gj.linestring_b, self.gj.linestring_a)
        self.assertTrue(b)

    def test_multiline_in_self(self):
        b = geometry_contains_other_geometry(self.gj.multilinestring, self.gj.multilinestring)
        self.assertTrue(b)

    def test_point_in_multiline(self):
        b = geometry_contains_other_geometry(self.gj.multilinestring, self.gj.point_a)
        self.assertTrue(b)

    def test_point_not_in_multiline(self):
        b = geometry_contains_other_geometry(self.gj.multilinestring, self.gj.point_b)
        self.assertFalse(b)

    def test_line_in_multiline(self):
        b = geometry_contains_other_geometry(self.gj.multilinestring, self.gj.linestring_a)
        self.assertTrue(b)

    def test_multiline_in_line(self):
        b = geometry_contains_other_geometry(self.gj.linestring_a, self.gj.multilinestring)
        self.assertFalse(b)

    def test_point_in_triangle(self):
        b = geometry_contains_other_geometry(self.gj.polygon_a, self.gj.point_a)
        self.assertTrue(b)

    def test_triangle_in_point(self):
        b = geometry_contains_other_geometry(self.gj.point_a, self.gj.polygon_a)
        self.assertFalse(b)

    def test_line_in_triangle(self):
        b = geometry_contains_other_geometry(self.gj.polygon_a, self.gj.linestring_a)
        self.assertFalse(b)

    def test_multipoint_in_multitriangle(self):
        b = geometry_contains_other_geometry(self.gj.multipolygon, self.gj.multipoint)
        self.assertTrue(b)

    def test_square_in_self(self):
        b = geometry_contains_other_geometry(self.gj.polygon_c, self.gj.polygon_c)
        self.assertTrue(b)

    def test_point_in_square(self):
        b = geometry_contains_other_geometry(self.gj.polygon_c, self.gj.point_b)
        self.assertTrue(b)

    def test_triangle_in_square(self):
        b = geometry_contains_other_geometry(self.gj.polygon_c, self.gj.polygon_b)
        self.assertTrue(b)

    def test_line_in_square(self):
        b = geometry_contains_other_geometry(self.gj.polygon_c, self.gj.linestring_d)
        self.assertTrue(b)

    def test_line_not_in_square(self):
        b = geometry_contains_other_geometry(self.gj.polygon_c, self.gj.linestring_b)
        self.assertFalse(b)

    def test_multipoint_in_square(self):
        b = geometry_contains_other_geometry(self.gj.polygon_c, self.gj.multipoint)
        self.assertFalse(b)

    def test_collection_in_square(self):
        b = geometry_contains_other_geometry(self.gj.polygon_c, self.gj.geo_collection)
        self.assertTrue(b)

    def test_collection_in_self(self):
        b = geometry_contains_other_geometry(self.gj.geo_collection, self.gj.geo_collection)
        self.assertTrue(b)

    def test_triangle_in_collection(self):
        b = geometry_contains_other_geometry(self.gj.geo_collection, self.gj.polygon_b)
        self.assertTrue(b)

    def test_square_in_collection(self):
        b = geometry_contains_other_geometry(self.gj.geo_collection, self.gj.polygon_c)
        self.assertFalse(b)

    # TODO: test (multi)lines with gaps and their complement(s)
