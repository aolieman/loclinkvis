var app = angular.module('reviewApp',['ui.bootstrap']);

app.controller("StatementCtrl", function ($scope, $timeout, $http) {
    $scope.initdata = function() {
        // TODO: replace objects that are used as hashmaps with Map
        // Containers for spotted toponyms
        $scope.initToponymContainers();
        // Hashmap of features by ObjectID
        $scope.featureMap = {};
        $scope.topCandidateOids = new Set();
        
        // Toggle menu
        $scope.menu = {
            isShowing: false, 
            selectedToponym: null,
			toponymInput: {},
			urlInput: {},
			textInput: {}
        };
        $scope.menu.toggle = function() {
            $scope.menu.isShowing = !$scope.menu.isShowing;
        };
        $scope.menu.select = function(toponym) {
            if ($scope.menu.selectedToponym === toponym) {
                $scope.menu.selectedToponym = null;
            } else {
                $scope.menu.selectedToponym = toponym;
            }
        };
        $scope.menu.isSelected = function(toponym) {
            return $scope.menu.selectedToponym === toponym;
        };
		$scope.menu.toponymInput.isLoading = false;
		$scope.menu.urlInput.isLoading = false;
		$scope.menu.textInput.isLoading = false;
		
		// Some example url inputs
		$scope.menu.urlInput.examples = [
			"https://nl.wikipedia.org/wiki/Hogesnelheidslijn_Schiphol_-_Antwerpen",
			"http://www.hofjesinamsterdam.nl/",
			"https://nl.wikipedia.org/wiki/Geschiedenis_van_Dordrecht",
			"http://www.visitleiden.nl/nl/ontdek-leiden/leiden-highlights/leidse-hofjes",
			"https://nl.wikipedia.org/wiki/Beleg_van_Leiden_(1573-1574)",
			"http://www.synbiosys.alterra.nl/natura2000/gebiedendatabase.aspx?subj=n2k&groep=4&id=n2k78"
		];
        
        // Initialize scope data from server
        $http.get('/location/names').success(function(response){
            $scope.locationNames = response;
           	$scope.map = L.map('map', {
            	fullscreenControl: true,
  				fullscreenControlOptions: {
    			position: 'topleft'
  				}
			}).setView([52.253, 5.765], 8);
            $scope.tileLayer = L.tileLayer('https://api.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png64?access_token={token}', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
                    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                    'Imagery &copy; <a href="http://mapbox.com">Mapbox</a>',
                mapid: 'alioli.ne5hhkd9',
                token: 'pk.eyJ1IjoiYWxpb2xpIiwiYSI6ImNpZWhnc2tnZTAwMXh0Ym04Ym54enBqejMifQ.Mm4nRNpQ1p3LmRr5QH5F7A'
            }).addTo($scope.map);
			var geojsonMarkerOptions = {
				radius: 5,
				weight: 0.5,
				opacity: 1,
				fillOpacity: 0.4
			};
            $scope.featureLayer = L.geoJson(null, {
				pointToLayer: function (feature, latlng) {
					return L.circleMarker(latlng, geojsonMarkerOptions);
				}
			}).addTo($scope.map);
            
            $('#loader').fadeOut(513, function(){
                $('#loader').remove();
            });
        }).error(function(response, status){
                $('#loadmsg').text("Loading Failed: HTTP Status "+status)
        });
    };
    
    /* Disable and enable Zoom*/
    $scope.disableMapControls = function() {
        if ($scope.map) {
            $scope.map.dragging.disable();
            $scope.map.touchZoom.disable();
            $scope.map.scrollWheelZoom.disable();
            $scope.map.doubleClickZoom.disable();
        }
    };
    $scope.enableMapControls = function() {
        if ($scope.map) {
            $scope.map.dragging.enable();
            $scope.map.touchZoom.enable();
            $scope.map.scrollWheelZoom.enable();
            $scope.map.doubleClickZoom.enable();
        }
    };


    /* Get features and add them to the map */
    $scope.getFeatureByName = function(toadd){
		$scope.menu.toponymInput.isLoading = true;
        $http.get('/location/' + toadd).success(function(featureCollection){
            $scope.featureLayer.addData(featureCollection);
        });
        $scope.selected = "";
		$scope.menu.toponymInput.isLoading = false;
    };
	$scope.getFeaturesFromUrl = function(url){
		$scope.menu.urlInput.isLoading = true;
		$http.post('/features_from_text', {'url': url}).success(function(resp){
            $scope.showResults(resp);
            $scope.inputUrl = "";
			$scope.menu.urlInput.isLoading = false;
        });
	};
    $scope.getFeaturesFromText = function(text){
		$scope.menu.textInput.isLoading = true;
        $http.post('/features_from_text', {'text': text}).success(function(resp){
            $scope.showResults(resp);
            $scope.inputText = "";
			$scope.menu.textInput.isLoading = false;
        });
    };
	$scope.showResults = function(resp){
		// Add new features to $scope
		angular.extend($scope.featureMap, resp.candidateFeatures);
		
		var featureCollection = {
			'type': "FeatureCollection",
			'features': []
		};
		
		// Add disambiguated and faux postings to their respective containers
		Object.keys(resp.disambiguatedToponyms).forEach(function(key) {
			$scope.disambiguatedToponyms[key] = $scope.disambiguatedToponyms.get(key).concat(resp.disambiguatedToponyms[key]);
			// Add the top candidate for each mention to featureCollection and $scope if it is new
			resp.disambiguatedToponyms[key].forEach(function(mention) {
				var topCandidateOid = mention.scored_candidates[0][1];
				if (!$scope.topCandidateOids.has(topCandidateOid)) {
					featureCollection.features.push($scope.featureMap[topCandidateOid]);
					$scope.topCandidateOids.add(topCandidateOid);
				}
			});
		});
		Object.keys(resp.fauxToponyms).forEach(function(key) {
			$scope.fauxToponyms[key] = $scope.fauxToponyms.get(key).concat(resp.fauxToponyms[key]);
		});
		
		$scope.featureLayer.addData(featureCollection);
		$scope.menu.selectedToponym = null;
	};
    
    /* Reset the interface to a clean state */
    $scope.clearNamesAndFeatures = function(){
        $scope.initToponymContainers();
        $scope.featureLayer.clearLayers();
    };
    
    $scope.initToponymContainers = function(){
        function getValueOrArray(obj, key) {
            if (obj.hasOwnProperty(key)) return obj[key];
            else return []
        }
        $scope.disambiguatedToponyms = {};
        $scope.disambiguatedToponyms.get = function(name){
            return getValueOrArray($scope.disambiguatedToponyms, name)
        };
        $scope.fauxToponyms = {};
        $scope.fauxToponyms.get = function(name){
            return getValueOrArray($scope.fauxToponyms, name)
        };
    };
});
