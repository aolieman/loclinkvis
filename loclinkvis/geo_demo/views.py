import json
import re

import requests
from bs4 import BeautifulSoup, Comment
from bson import json_util
from flask import request

from loclinkvis.gazetteer.models import Feature
from loclinkvis.geocoding.disambiguation import Disambiguator
from loclinkvis.geocoding.location_name_tree import LocationNameSearcher, get_toponym_postings
from loclinkvis.utils.db import get_feature_collection
from loclinkvis.web_app import app

json_headers = {'Content-Type': 'application/json'}


# Initialize components
all_names = Feature.collection.distinct('known_names')
lns = LocationNameSearcher(all_names)
del all_names
dtor = Disambiguator()


@app.route('/')
def root():
    return app.send_static_file('index.html')


@app.route('/biographies')
def biographies():
    return app.send_static_file('biographies.html')


@app.route('/static/<path:path>')
def static_proxy(path):
    # send_static_file will guess the correct MIME type
    return app.send_static_file(path.replace('\\', '/'))


@app.route('/fonts/<path:path>')
def static_fonts_proxy(path):
    # send_static_file will guess the correct MIME type
    return app.send_static_file('fonts/' + path.replace('\\', '/'))


@app.route('/biographies/static/<path:path>')
def static_bio_proxy(path):
    # send_static_file will guess the correct MIME type
    return app.send_static_file(path.replace('\\', '/'))


@app.route('/location/names', methods=['GET'])
def get_location_names():
    # TODO: remove disambiguation suffixes, e.g. (NB), from gazetteer
    location_names = Feature.collection.distinct('known_names')
    body_str = json.dumps(location_names)
    return body_str, 200, json_headers


@app.route('/location/<path:name>', methods=['GET'])
def get_features_by_name(name):
    feature_collection = get_feature_collection({'known_names': name})
    body_str = json.dumps(feature_collection)
    return body_str, 200, json_headers


@app.route('/features_from_text', methods=['POST'])
def get_features_from_text():
    posted_data = request.get_json()
    if posted_data.get('text'):
        text = posted_data['text']
    elif posted_data.get('url'):
        text = get_text_from_url(posted_data['url'])
    else:
        body_str = json.dumps({
            'inputError': 'post either "text" or "url" to this method'
        })
        return body_str, 400, json_headers

    valid, faux = lns.spot_toponyms(text, details=True)
    faux_postings = get_toponym_postings(faux)

    dmatches = dtor.disambiguate_toponyms(valid, text, details=True)
    dpostings, cand_features = dtor.get_candidates_per_toponym(dmatches)

    body_str = json.dumps({
        'disambiguatedToponyms': dpostings,
        'fauxToponyms': faux_postings,
        'candidateFeatures': cand_features
    }, default=json_util.default)

    return body_str, 200, json_headers


@app.route('/features_from_static/<pm_id>', methods=['GET'])
def get_features_from_static(pm_id):
    with open('biographies/{}.txt'.format(pm_id), 'r') as f:
        text = f.read().decode('cp1252')
    valid, faux = lns.spot_toponyms(text, details=True)
    valid_postings = get_toponym_postings(valid)
    faux_postings = get_toponym_postings(faux)

    feature_collection = get_feature_collection({
        'known_names': {'$in': valid_postings.keys()}
    })
    body_str = json.dumps({
        'spottedToponyms': valid_postings,
        'fauxToponyms': faux_postings,
        'featureCollection': feature_collection,
        'pmId': pm_id
    })
    return body_str, 200, json_headers


##
# Helper functions
##
def get_text_from_url(url):
    resp = requests.get(url)

    if resp.headers.get('content-type') and 'text/html' in resp.headers['content-type']:
        soup = BeautifulSoup(resp.text, 'lxml')
        text_contents = soup.body.find_all(text=True)
        visible_elements = filter(is_visible, text_contents)
        concatenated_text = ''.join(visible_elements).strip()

        return re.sub(r'\n(\n+)', '\n\n', concatenated_text)

    else:
        return resp.text


def is_visible(element):
    if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
        return False
    elif isinstance(element, Comment):
        return False
    return True
