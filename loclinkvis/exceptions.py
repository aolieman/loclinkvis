
class LocLinkVisError(Exception):
    pass


class CannotSave(LocLinkVisError):
    pass


class CredentialsInvalid(LocLinkVisError):
    pass


class NameSearcherError(LocLinkVisError):
    pass

class DbConnectionError(LocLinkVisError):
    pass
