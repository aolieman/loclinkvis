Nederland_ Mag ik zelf nog even terug naar Nederland?

Nederland_ De heer Wöltgens: Ik neem aan Nederland in SUPERNL en op de wereldkaart.

Nederland_ Omdat ik het gehele verhaal wil doorprikken, dat Nederland boven alles zweeft als een engeltje.

Israël_ Collega Pechtold zegt dat *COUNTRY* zich moet inzetten om alsnog verontschuldigingen van Israël te bewerkstelligen. 

Nederland_ Collega Pechtold zegt dat Nederland zich moet inzetten om alsnog verontschuldigingen van *COUNTRY* te bewerkstelligen.
 
Nederland_ Ik denk echter dat het een rol voor Nederland zou kunnen zijn om te proberen *COUNTRY* en *COUNTRY* gezamenlijk weer zaken te laten doen in projecten waarbij ook *COUNTRY* betrokken is, los van dit conflict, van het konvooi en van wat in het verleden is geweest. *COUNTRY* en deze minister houden van zakendoen. *COUNTRY* was in 2011 voor *COUNTRY* de derde handelspartner. *COUNTRY* is goed in handeldrijven. 

Nederland_ *COUNTRY* wil graag dat de Marokkanen in Nederland en in andere landen onderwijs krijgen in de eigen taal en cultuur. 

Nederland_ *COUNTRY* speelt alleen een bemiddelende en hulpverlenende rol. Wij kunnen die erkennen en accepteren, aangezien de scholen in Nederland alle vrijheid behouden. 

Nederland_ Ik wil ontzettend graag precies weten waarom het gaat. Het gaat o m een in Nederland wonende aandeelhouder van een in *COUNTRY* gevestigde maatschappij, welke op haar beurt een 100%dochter heeft in *COUNTRY*, zijnde een naar *COUNTRY* recht opgerichte maatschappij. De aandeelhouder is aandeelhouder in de moedermaatschappij. 

Nederland_ Daarom zoeken wij in Nederland naar alternatieve straffen!

Nederland_ Mijnheer de Voorzitter! Er bestaat natuurlijk geen bepaalde militair-technische reden voor dat dat boven Nederland gebeurt. Dat is gewoon een verdeling van de oefenmogelijkheden binnen het NAVO-verband, waaraan *COUNTRY* ook deelneemt. Het is als het ware een kwestie van lastenverdeling.

Belgie_ Waarde collega's. Tijdens het zomer– reces is Koning Boudewijn van Belgie plotseling overleden Namens de Kamer heb ik aan de voorzitter van de Kamer vanvolksvertegen– woordigers, de heer Nothomb, een telegram van rouwbeklag gezonden met de volgende inhoud:

Belgie_ Zal de Zuidafrikaanse president De Klerk deze maand gedurende drie weken bezoeken brengen aan acht of wellicht negen — Europese landen, te weten *COUNTRY*, het *COUNTRY*, de Bondsrepubliek *COUNTRY*, Belgie. *COUNTRY*, *COUNTRY*, *COUNTRY* en *COUNTRY* en aan de EG-commissie?

Zwitserland_ Zal de Zuidafrikaanse president De Klerk deze maand gedurende drie weken bezoeken brengen aan acht of wellicht negen — Europese landen, te weten *COUNTRY*, het *COUNTRY*, de Bondsrepubliek *COUNTRY*, *COUNTRY*. *COUNTRY*, *COUNTRY*, *COUNTRY* en Zwitserland en aan de EG-commissie?

Italië_ Zal de Zuidafrikaanse president De Klerk deze maand gedurende drie weken bezoeken brengen aan acht of wellicht negen — Europese landen, te weten *COUNTRY*, het *COUNTRY*, de Bondsrepubliek *COUNTRY*, *COUNTRY*. *COUNTRY*, *COUNTRY*, Italië en *COUNTRY* en aan de EG-commissie?

Portugal_ Zal de Zuidafrikaanse president De Klerk deze maand gedurende drie weken bezoeken brengen aan acht of wellicht negen — Europese landen, te weten *COUNTRY*, het *COUNTRY*, de Bondsrepubliek *COUNTRY*, *COUNTRY*. *COUNTRY*, Portugal, *COUNTRY* en *COUNTRY* en aan de EG-commissie?

Frankrijk_ Zal de Zuidafrikaanse president De Klerk deze maand gedurende drie weken bezoeken brengen aan acht of wellicht negen — Europese landen, te weten Frankrijk, het *COUNTRY*, de Bondsrepubliek *COUNTRY*, *COUNTRY*. *COUNTRY*, *COUNTRY*, *COUNTRY* en *COUNTRY* en aan de EG-commissie?

Belgie_ druk op haar heeft uitgeoefend om dat nu te doen met Vlaanderen, maar dat doen ze al twee of drie jaar, dat weet u net zo goed als ik. U had net zo goed even tot eind mei kunnen wachten, wanneer het verdrag binnen Belgie over de bevoegdheden tussen de gewesten en de staat wordt getekend. Als u even had afgewacht wat daar uitkwam, had u kunnen beslissen of u een verdrag of een overeenkomst wilde sluiten. Ik vind uw reactie echt prematuur.

Luxemburg_ aanteken dat ik niet uitsluit dat TV10 als ontduiking van de Nederlandse Mediawet een in Luxemburg illegale omroep zou opleveren en dus in *COUNTRY* geweerd zou moeten worden.

Luxemburg_ aanteken dat ik niet uitsluit dat TV10 als ontduiking van de Nederlandse Mediawet een in *COUNTRY* illegale omroep zou opleveren en dus in Luxemburg geweerd zou moeten worden.

België_ Gold met betrekking tot België alleen de tijdsfactor?

Luxemburg_ De heer Eisma (D66) Dat gold met name Luxemburg!

Luxemburg_ Met respect voor het land Luxemburg: *COUNTRY* is toch niet Luxemburg?

Zuid-Afrika_ Met respect voor het land *COUNTRY*: Zuid-Afrika is toch niet Luxemburg?