from setuptools import setup

setup(
    name='loclinkvis',
    packages=['loclinkvis'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
